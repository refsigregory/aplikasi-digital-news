<?php namespace App\Models;

use CodeIgniter\Model;

class PenggunaModel extends Model
{
    protected $table = 'pengguna';

    protected $primaryKey = 'id_pengguna';

    protected $allowedFields = ['id_pengguna', 'username', 'password', 'email', 'nama_lengkap', 'tanggal_lahir', 'peran','status_pengguna', 'kode_verifikasi'];

    public function get($id = false)
    {
            if ($id === false)
            {
                    return $this->asObject()->findAll();
            }

            return $this->asObject()
                        ->where(['id_pengguna' => $id])
                        ->first();
    }
    

}