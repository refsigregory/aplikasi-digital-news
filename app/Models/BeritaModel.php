<?php namespace App\Models;

use CodeIgniter\Model;

class BeritaModel extends Model
{
    protected $table = 'berita';

    protected $primaryKey = 'id_berita';

    protected $allowedFields = ['id_berita', 'thumbnail_berita', 'file_berita', 'edisi', 'halaman', 'penerbit', 'waktu','harga', 'status_berita', 'diposting_oleh'];

    public function get($id = false)
    {
            if ($id === false)
            {
                    return $this->asObject()->findAll();
            }

            return $this->asObject()
                        ->where(['id_berita' => $id])
                        ->first();
    }
    

}