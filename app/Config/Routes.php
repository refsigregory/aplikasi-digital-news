<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('/login', 'Home::login');
$routes->get('/register', 'Home::register');
$routes->get('/verif/(:any)', 'Home::verif/$1');
$routes->get('/ubah_profil', 'Home::ubah_profil');

$routes->get('/iklan', 'Home::iklan');
$routes->get('/tambah_iklan', 'Home::tambah_iklan');
$routes->get('/edit_iklan/(:any)', 'Home::edit_iklan/$1');
$routes->get('/hapus_iklan/(:any)', 'Home::delete_iklan/$1');

$routes->get('/berita', 'Home::berita');
$routes->get('/tambah_berita', 'Home::tambah_berita');
$routes->get('/edit_berita/(:any)', 'Home::edit_berita/$1');
$routes->get('/hapus_berita/(:any)', 'Home::delete_berita/$1');

$routes->get('/pengguna', 'Home::pengguna');
$routes->get('/tambah_pengguna', 'Home::tambah_pengguna');
$routes->get('/edit_pengguna/(:any)', 'Home::edit_pengguna/$1');
$routes->get('/hapus_pengguna/(:any)', 'Home::delete_pengguna/$1');

$routes->get('/logout', 'Home::logout');

$routes->post('/login', 'Home::check_login');
$routes->post('/register', 'Home::check_register');
$routes->post('/verif_akun', 'Home::verif_akun');
$routes->post('/ubah_profil', 'Home::save_pengguna');
$routes->post('/tambah_pengguna', 'Home::add_pengguna');
$routes->post('/tambah_berita', 'Home::add_berita');
$routes->post('/tambah_iklan', 'Home::add_iklan');
$routes->post('/edit_berita', 'Home::save_berita');
$routes->post('/edit_iklan', 'Home::save_iklan');
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
