<?php namespace App\Database\Seeds;
  
class PenggunaSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data1 = [
            'username'     => 'admin',
            'password'   => 'admin',
            'nama_lengkap' => 'Administrator',
            'email' => 'admin@localhost',
            'status_pengguna' => 'aktif',
            'peran' => 'admin',
        ];
        $data2 = [
            'username'     => 'redaksi',
            'password'   => 'redaksi',
            'nama_lengkap' => 'Asisten Redaksi',
            'email' => 'redaksi@localhost',
            'status_pengguna' => 'aktif',
            'peran' => 'asisten_redaksi'
        ];
        $data3 = [
            'username'     => 'pegawai',
            'password'   => 'pegawai',
            'nama_lengkap' => 'Pegawai Redaksi',
            'email' => 'redaksi@localhost',
             'status_pengguna' => 'aktif',
            'peran' => 'pegawai_redaksi'
        ];
        $data4 = [
            'username'     => 'pengiklan',
            'password'   => 'pengiklan',
            'nama_lengkap' => 'Pengiklan',
             'status_pengguna' => 'aktif',
            'email' => 'pengiklan@localhost',
            'peran' => 'pengiklan'
        ];
        $this->db->table('pengguna')->insert($data1);
        $this->db->table('pengguna')->insert($data2);
        $this->db->table('pengguna')->insert($data3);
        $this->db->table('pengguna')->insert($data4);
    }
} 