<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pengguna extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id_pengguna'           => [
				'type'              => 'INT',
				'constraint'        => 20,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'username'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'password'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'email'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'nama_lengkap'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'tanggal_lahir'         => [
				'type'              => 'DATE'
			],
			'kode_verifikasi'       => [
				'type'              => 'VARCHAR',
				'constraint'		=> '20'
			],
			'status_pengguna'       => [
				'type'              => 'ENUM',
				'constraint'        => "'aktif','nonaktif'",
				'default'           => 'nonaktif'
			],
			'peran'       => [
				'type'              => 'ENUM',
				'constraint'        => "'admin','pengiklan','asisten_redaksi','pegawai_redaksi'",
				'default'           => 'admin'
			],
		]);
		$this->forge->addKey('id_pengguna', TRUE);
		$this->forge->createTable('pengguna');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
