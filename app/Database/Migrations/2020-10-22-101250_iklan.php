<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Iklan extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id_iklan'           => [
				'type'              => 'INT',
				'constraint'        => 20,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'deskirpsi_iklan'         => [
				'type'              => 'TEXT'
			],
			'gambar_iklan'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'waktu_mulai'         => [
				'type'              => 'DATETIME'
			],
			'waktu_selesai'         => [
				'type'              => 'DATETIME'
			],
			'diposting_oleh'           => [
				'type'              => 'INT',
				'constraint'        => 20
			],
		]);
		$this->forge->addKey('id_iklan', TRUE);
		$this->forge->createTable('iklan');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
