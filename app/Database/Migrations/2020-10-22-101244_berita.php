<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Berita extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id_berita'           => [
				'type'              => 'INT',
				'constraint'        => 20,
				'unsigned'          => TRUE,
				'auto_increment'    => TRUE
			],
			'thumbnail_berita'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'file_berita'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'edisi'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'halaman'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'penerbit'         => [
				'type'              => 'VARCHAR',
				'constraint'        => '100',
			],
			'waktu'         => [
				'type'              => 'DATETIME'
			],
			'harga'       => [
				'type'              => 'DOUBLE'
			],
			'status_berita'         => [
				'type'              => 'ENUM',
				'constraint'        => "'aktif','nonaktif'"
			],
			'diposting_oleh'           => [
				'type'              => 'INT',
				'constraint'        => 20
			],
		]);
		$this->forge->addKey('id_berita', TRUE);
		$this->forge->createTable('berita');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
