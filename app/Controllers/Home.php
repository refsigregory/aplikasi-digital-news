<?php namespace App\Controllers;

use App\Models\PenggunaModel;
use App\Models\BeritaModel;
use Imagick;

class Home extends BaseController
{
	public $email;
	public $emailFrom;
	public $emailName;
	public $verifMail;
	public $pathUpload;

	public function __construct()
	{
		$this->email = \Config\Services::email();
		$this->verifMail = true; // jika ingin aktifkan verfi email (harus konfigurasi dulu)
		$this->emailFrom = ''; // isi email
		$this->emailName = 'Digital News';

		$this->pathUpload = '/home/projects/web/digital-news.projects.ref.si/public_html/public/files/uploads/'; // lokasi upload file

		//$this->file = new \CodeIgniter\Files\File($path);

		$this->pengguna = new PenggunaModel();
		$this->berita = new BeritaModel();
	}

	public function index()
	{
		$data['session'] = $this->session;
		$data['berita'] = $this->berita->get();

		echo view('header', $data);
		echo view('content');
		echo view('footer');
	}

	public function login()
	{
		$data['session'] = $this->session;

		echo view('header', $data);
		echo view('pages/login');
		echo view('footer');
	}

	public function register()
	{
		$data['session'] = $this->session;

		echo view('header', $data);
		echo view('pages/register');
		echo view('footer');
	}

	public function verif($email)
	{
		$data['session'] = $this->session;
		$data['email'] = $email;

		echo view('header', $data);
		echo view('pages/verif_akun', $data);
		echo view('footer');
	}

	public function home()
	{
		return redirect()->to('/');
	}

	public function pengguna()
	{
		$data['session'] = $this->session;
		$data['pengguna'] = $this->pengguna->get();

		echo view('header', $data);
		echo view('pages/pengguna', $data);
		echo view('footer');
	}

	public function tambah_pengguna()
	{
		$data['session'] = $this->session;

		echo view('header', $data);
		echo view('pages/tambah_pengguna', $data);
		echo view('footer');
	}

	public function edit_pengguna($id)
	{
		$data['session'] = $this->session;
		$data['user'] = $this->pengguna->get($id);

		echo view('header', $data);
		echo view('pages/edit_pengguna', $data);
		echo view('footer');
	}

	public function berita()
	{
		$data['session'] = $this->session;
		$data['berita'] = $this->berita->get();

		echo view('header', $data);
		echo view('pages/berita', $data);
		echo view('footer');
	}

	public function tambah_berita()
	{
		$data['session'] = $this->session;

		echo view('header', $data);
		echo view('pages/tambah_berita', $data);
		echo view('footer');
	}

	public function edit_berita($id)
	{
		$data['session'] = $this->session;
		$data['berita'] = $this->berita->get($id);

		echo view('header', $data);
		echo view('pages/edit_berita', $data);
		echo view('footer');
	}

	public function ubah_profil()
	{
		$data['session'] = $this->session;
		$data['user'] = $this->pengguna->get($this->session->get('id'));

		echo view('header', $data);
		echo view('pages/ubah_profil', $data);
		echo view('footer');
	}
	

	// =====================

	public function check_login()
	{
		$username 		= $_POST['username'];
		$password	= $_POST['password'];
		$query = $this->db->query("select * from pengguna where username = '$username' and password = '$password'");
		if($query->resultID->num_rows > 0)
		{
			$user = $query->getResult()[0];

			$data = [
				"id" 	=> $user->id_pengguna,
				"name"	  	=> $user->nama_lengkap,
				"email"	  	=> $user->email,
				"peran" => $user->peran
			];

			if($user->status_pengguna =="aktif"):
				$this->session->set($data);

				return redirect()->to('home');
			else:
				$msg = [
					"type" => "error",
					"text" => "Verfikasi Akun Anda, Masukkan kode yang telah di kirim ke email Anda (".$user->email.")" 
				];
				$this->session->setFlashdata('msg', $msg);
				return redirect()->to('verif/'.$user->email);
			endif;

		} else {
			$msg = [
				"type" => "error",
				"text" => "Email atau Password tidak cocok!" 
			];
			$this->session->setFlashdata('msg', $msg);
			return redirect()->to('/login');
		}
	}

	public function check_register()
	{
		$email 		= $_POST['email'];
		$query = $this->db->query("select * from pengguna where email = '$email' ");
		$data = $_POST;
		if($query->resultID->num_rows <= 0)
		{
			$kode = rand(1000,9999);
			if($this->verifMail != true) {
				$data = array_merge($data, ["kode_verifikasi" => $kode,"peran" => "pengiklan","status_pengguna"=>"aktif"]);
			} else {
				$data = array_merge($data, ["kode_verifikasi" => $kode,"peran" => "pengiklan"]);
			}
			
			//print_r($data);
			$this->pengguna->insert($data);

			if($this->verifMail == true) {
				$mail = "
					Kode Verifikasi Anda adalah ".$kode."
				";
				
				$this->sendMail($data['email'], "Kode Verfikasi Akun", $mail);
			}


			$err =  "Pendaftaran berhasil";
			$msg = [
				"type" => "success",
				"text" => $err 
			];
			$this->session->setFlashdata('msg', $msg);

			return redirect()->to('/login');

		} else {
			$msg = [
				"type" => "error",
				"text" => "Email sudah ada!" 
			];
			$this->session->setFlashdata('msg', $msg);
			return redirect()->to('/register');
		}
	}

	public function save_pengguna()
	{
		$email 		= $_POST['email'];
		$id 		= $_POST['id_pengguna'];
		//$query = $this->db->query("select * from pengguna where email = '$email' ");
		$data = $_POST;
		unset($data['id_pengguna']);
		if(true)//$query->resultID->num_rows <= 0)
		{
			//print_r($data);
			$this->pengguna->update($id,$data);

			$err =  "Data berhasil disimpan";
			$msg = [
				"type" => "success",
				"text" => $err 
			];
			$this->session->setFlashdata('msg', $msg);

			return redirect()->to('/');

		} else {
			$msg = [
				"type" => "error",
				"text" => "Email sudah ada!" 
			];
			$this->session->setFlashdata('msg', $msg);
			return redirect()->to('/');
		}
	}

	public function add_pengguna()
	{
		$email = $_POST['email'];
		$query = $this->db->query("select * from pengguna where email = '$email' ");
		$data = $_POST;
		if($query->resultID->num_rows <= 0)
		{

			$data = array_merge($data, ["status_pengguna"=>"aktif"]);
			
			$this->pengguna->insert($data);


			$err =  "Pengguna berhasil ditambah";
			$msg = [
				"type" => "success",
				"text" => $err 
			];
			$this->session->setFlashdata('msg', $msg);

			return redirect()->to('/pengguna');

		} else {
			$msg = [
				"type" => "error",
				"text" => "Email sudah ada!" 
			];
			$this->session->setFlashdata('msg', $msg);
			return redirect()->to('/tambah_pengguna');
		}
	}

	public function delete_pengguna($id)
	{
		//$query = $this->db->query("select * from pengguna where email = '$email' ");

		if(true)//$query->resultID->num_rows <= 0)
		{
			$this->pengguna->delete($id);
			return redirect()->to('/pengguna');
		}
	}

	public function delete_berita($id)
	{
		//$query = $this->db->query("select * from pengguna where email = '$email' ");

		if(true)//$query->resultID->num_rows <= 0)
		{
			$this->berita->delete($id);
			return redirect()->to('/berita');
		}
	}

	public function delete_iklan($id)
	{
		//$query = $this->db->query("select * from pengguna where email = '$email' ");

		if(true)//$query->resultID->num_rows <= 0)
		{
			$this->iklan->delete($id);
			return redirect()->to('/iklan');
		}
	}

	public function verif_akun()
	{
		$email 		= $_POST['email'];
		$kode 		= $_POST['kode'];
		$query = $this->db->query("select * from pengguna where email = '$email' ");
		if($query->resultID->num_rows > 0)
		{
			$user = $query->getResult()[0];
			if($user->kode_verifikasi == $kode){
				$this->pengguna->update($user->id_pengguna,["status_pengguna"=>"aktif"]);
				$err =  "Verifikasi berhasil, silahkan masuk";
				$msg = [
					"type" => "success",
					"text" => $err 
				];
				$this->session->setFlashdata('msg', $msg);
			} else {
				$err =  "Kode Verfikasi Salah!";
				$msg = [
					"type" => "error",
					"text" => $err 
				];
				$this->session->setFlashdata('msg', $msg);
			}
		} else {
			$err =  "Verifikasi gagal";
			$msg = [
				"type" => "error",
				"text" => $err 
			];
			$this->session->setFlashdata('msg', $msg);
		}

		return redirect()->to('/login');
	}

	public function add_berita()
	{
		$data = $_POST;

            if(!empty($data))
            {
                $foto = $this->request->getFile('file_berita');

                if ($foto->isValid() && ! $foto->hasMoved())
                {

                    $newName = $foto->getRandomName();
                    $foto->move('files/uploads', $newName);

                    unset($data['file_berita']);
                    foreach($data as $key=>$value)
                    {
                        if($value == '')
                            $err .= ucfirst(str_replace("_", " ", $key)) . " tidak boleh kosong! <br>";
                    }

					/*
						sudo apt install php-imagick
						sudo apt install ghostscript
						set policy.xml PDF read|write
					 */
					$path = '/home/projects/web/digital-news.projects.ref.si/public_html/public/files/uploads/';

					
						$pdf_file = $path.$newName;//'/home/projects/web/digital-news.projects.ref.si/public_html/public/files/uploads/1603441276_decddb985665795b79a8.pdf'; 
						
						if (! is_readable($pdf_file)) {
							echo 'file not readable';
							exit();
						}
						
					try {
						$im = new Imagick();
						$im->setResolution(300, 300);     //set the resolution of the resulting jpg
						$im->readImage($pdf_file.'[0]');    //[0] for the first page
						$im->setImageFormat('jpg');
						$thub_name = md5(time()).".jpg";
						$thub_path = $path.$thub_name;
						$im->writeImage ($thub_path);
					} catch(Exception $ex){
						exit($ex);
					}

					$temp_data = ["file_berita" => $newName, "waktu"=> date("Y-m-d H:i:s"),"thumbnail_berita"=>$thub_name,"status_berita"=>"aktif", "diposting_oleh"=>$this->session->get("id")];
					$data = array_merge($data,$temp_data);
					
					$this->berita->insert($data);
					//print_r($data);

                    $msg = [
                        "type" => "success",
                        "text" => "Data berhasil ditambah!" 
                    ];
                    $this->session->setFlashdata('msg', $msg);
                    
                } else {
                    $err =  $foto->getError();
                    $msg = [
                        "type" => "error",
                        "text" => $err 
                    ];
                    $this->session->setFlashdata('msg', $msg);
                }
            } else {
                $error = $err;
                $msg = [
                    "type" => "error",
                    "text" => $error
                ];
                $this->session->setFlashdata('msg', $msg);
            }
            return redirect()->to('/berita');
	}

	public function save_berita()
	{
		$id = $_POST['id_berita'];
		$data = $_POST;
		unset($data['id_berita']);
		$query = $this->db->query("select * from berita where id_berita = '$id' ");
		if($query->resultID->num_rows > 0)
		{
			//print_r($data);
			if(!isset($data['ganti_file'])) {
				$data['ganti_file'] = "";
			}

			if($data['ganti_file'] != ""){
				$foto = $this->request->getFile('file_berita');

				if ($foto->isValid() && ! $foto->hasMoved())
				{
					$newName = $foto->getRandomName();
					$foto->move('files/uploads', $newName);

					unset($data['file_berita']);
					foreach($data as $key=>$value)
					{
						if($value == '')
							$err .= ucfirst(str_replace("_", " ", $key)) . " tidak boleh kosong! <br>";
					}

					/*
						sudo apt install php-imagick
						sudo apt install ghostscript
						set policy.xml PDF read|write
					*/
					$path = $this->pathUpload;

					
						$pdf_file = $path.$newName;
						
						if (! is_readable($pdf_file)) {
							echo 'file not readable';
							exit();
						} else {
							// hapus file lama
							if (is_readable($path.$data['file_berita_lama'])) {
								unlink($path.$data['file_berita_lama']);
							}
							if (is_readable($path.$data['thumbnail_berita'])) {
								unlink($path.$data['thumbnail_berita']);
							}
							
							unset($data['ganti_file']);
							unset($data['thumbnail_berita']);
						}
						
					try {
						$im = new Imagick();
						$im->setResolution(300, 300);     //set the resolution of the resulting jpg
						$im->readImage($pdf_file.'[0]');    //[0] for the first page
						$im->setImageFormat('jpg');
						$thub_name = md5(time()).".jpg";
						$thub_path = $path.$thub_name;
						$im->writeImage ($thub_path);
					} catch(Exception $ex){
						exit($ex);
					}

					$temp_data = ["file_berita" => $newName, "thumbnail_berita"=>$thub_name];
					$data = array_merge($data,$temp_data);
				
				}
			}

			unset($data['ganti_file']);

			$this->berita->update($id,$data);

			$err =  "Data berhasil disimpan";
			$msg = [
				"type" => "success",
				"text" => $err 
			];
			$this->session->setFlashdata('msg', $msg);

			return redirect()->to('/berita');

		} else {
			$msg = [
				"type" => "error",
				"text" => "Data tidak ada!" 
			];
			$this->session->setFlashdata('msg', $msg);
			return redirect()->to('/berita');
		}
	}

	public function sendMail($to, $subject, $message)
	{

		$this->email->setFrom($this->emailFrom,$this->emailName);
		$this->email->setTo($to);

		//$this->email->attach($attachment);

		$this->email->setSubject($subject);
		$this->email->setMessage($message);

		if(! $this->email->send()){
			return false;
		}else{
			return true;
		}
	}

	public function logout()
	{
		$this->session->destroy();
		return redirect()->to('/login');
	}

	//--------------------------------------------------------------------

}
