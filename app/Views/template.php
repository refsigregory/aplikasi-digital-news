<!DOCTYPE html>
<html lang="id">
<head>
    <title>Jawa Pos Digital</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Jawa Pos Digital" name="description" />
    <meta content="Jawa Pos Digital" name="author" />
    <meta name="csrf-token" content="xrPHUFBWHpg9LFAZYAxceq8RWfUmSrYxu7BWAFTD">

    
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="site-url" content="https://digital.jawapos.com/ "/>
    <meta name="robots" content="index,follow"/>
    <meta name="googlebot-news" content="index,follow"/>
    <meta name="googlebot" content="index,follow" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="google-site-verification" content="seySPhGGB84qmdGrnrE2Qy4-tHKZDl6rIJF5_YYPHbE" />
    <meta name="msvalidate.01" content="99BAF79DBBE6C67F4DCACF0C876CAC5A" />
    

    <link href="https://digital.jawapos.com/theme/css/new_style.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/responsive_style.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/text-version.css" rel="stylesheet" type="text/css">

    <link href="https://digital.jawapos.com/theme/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link href="https://digital.jawapos.com/theme/css/Hover-master/hover.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="icon" href="https://digital.jawapos.com/favicon.ico" type="image/x-icon"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">


    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MRP6DQN');</script>
    <!-- Start Alexa Certify Javascript -->
    <script type="text/javascript">
        _atrk_opts = { atrk_acct:"p/jsr1KAfD20Cs", domain:"jawapos.com",dynamic: true};
        (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
    </script>
    <noscript><img src="https://certify.alexametrics.com/atrk.gif?account=p/jsr1KAfD20Cs" style="display:none" height="1" width="1" alt="alexametrics" /></noscript>

</head>

<body style="background-color: #e5e5e5;">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MRP6DQN"
                  height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>


    <div id="app">
        <main-app/>
    </div>

</body>


<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script src="https://digital.jawapos.com/js/app.js"></script>
<script src="https://digital.jawapos.com/theme/js/jquery-3.3.1.min.js"></script>
<script src="https://digital.jawapos.com/theme/js/jquery-ui.js"></script>
<script src="https://digital.jawapos.com/theme/js/popper.min.js"></script>
<script src="https://digital.jawapos.com/theme/js/bootstrap.min.js"></script>
<script src="https://digital.jawapos.com/theme/js/owl.carousel.min.js"></script>
<script src="https://digital.jawapos.com/theme/js/jquery.magnific-popup.min.js"></script>
<script src="https://digital.jawapos.com/theme/js/aos.js"></script>
<script src="https://digital.jawapos.com/theme/js/main.js"></script>
<script src="https://digital.jawapos.com/front-theme/js/moment.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>



<script src="https://digital.jawapos.com/slider/examples-bootstrap/ie10-viewport-bug-workaround.js"></script>
<script src="https://digital.jawapos.com/slider/js/jssor.slider.min.js"></script>
<script src="https://app.midtrans.com/snap/snap.js" data-client-key="Mid-client-__qRg2s2lejLRjJO"></script>
</body>
</html>
