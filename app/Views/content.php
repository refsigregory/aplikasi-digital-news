<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h2>Akses Berita DigitalNews<br>Lebih Mudah di</h2>
    <h3><?=$_SERVER['HTTP_HOST'];?></h3>
  </div>
</div>
<div class="container">
<div class="card-columns">
  <?php if(count($berita) >0):
  foreach($berita as $row):?>
    <div class="card bg-default" >
    <a href="/files/uploads/<?=$row->file_berita;?>" target="_blank">
      <div class="card-body text-center">
        <p class="card-text"><img src="/files/uploads/<?=$row->thumbnail_berita;?>" style="width:90%; height: 400px"></p>
      </div>
    </a>
      <div class="text-center">
        <?=$row->edisi;?><br>
        <b><?=$row->penerbit;?></b>
      </div>
    </div>
  <?php endforeach;
  endif;
  ?>
  </div>
</div>