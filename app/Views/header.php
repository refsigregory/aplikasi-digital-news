<!DOCTYPE html>
<html lang="en">
<head>
  <title>Digital News</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
</head>
<body>
<div class="">

<nav class="navbar bg-light navbar-light sticky-top">
        <a class="navbar-brand" href="<?=base_url();?>">Digital News</a>

        <form class="form-inline">
  
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
      
        </form>
        
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url();?>">Home</a>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url('iklan');?>">Iklan</a>
            </li>
            <?php if($session->has('id')):?>
                <?php if($session->get('peran') == 'admin' || $session->get('peran') == 'kepala_redaksi' || $session->get('peran') == 'asisten_redaksi'):?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url('pengguna');?>">Pengguna</a>
                    </li>
                <?php endif;?>
                <?php if($session->get('peran') != 'pengiklan'):?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url('berita');?>">Berita</a>
                    </li>
                <?php endif;?>
                <?php if($session->get('peran') == 'pengiklan'):?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url('ubah_profil');?>">Ubah Profil</a>
                    </li>
                <?php endif;?>
                <li class="nav-item">
                   <a class="nav-link" href="/logout"><button type="button" class="btn btn-outline-success"> Logout</button></a>
                </li>
            <?php else: ?>
                <li class="nav-item">
                <a class="nav-link" href="/login"><button type="button" class="btn btn-outline-success">Login</button></a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/register"><button type="button" class="btn btn-outline-success">Register</button></a>
                </li>
            <?php endif;?>
        </ul>
    

</nav> 

<p class="login-box-msg">
    <?php
        if (!empty($session->getFlashdata('msg'))):
            $msg = $session->getFlashdata('msg');
    ?>
    <?php if($msg['type'] == 'success'): ?>
        <div class="alert alert-success"><?=$msg['text'];?></div>
    <?php elseif ($msg['type'] == 'warning'): ?>
        <div class="alert alert-warning"><?=$msg['text'];?></div>
    <?php elseif ($msg['type'] == 'error'): ?>
        <div class="alert alert-danger"><?=$msg['text'];?></div>
    <?php else: ?>
        <div class="alert alert-info"><?=$msg['text'];?></div>
    <?php endif; ?>
    <?php endif; ?>
</p>
