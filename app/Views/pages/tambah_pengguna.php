<form method="post" action="/tambah_pengguna">
<p>
<div class="card">
  <div class="card-header">Ubah Pengguna</div>
  <div class="card-body">
    <div class="form-group">
        <label for="username">Username:</label>
        <input type="username" name="username" class="form-control" placeholder="" id="username">
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" placeholder="" id="pwd">
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" class="form-control" placeholder="" id="email" >
    </div>

    <div class="form-group">
        <label for="tanggal_lahir">Peran:</label>
        <select name="peran" class="form-control">
            <option value="pengiklan">Pengiklan</option> 
            <option value="pegawai_redaksi">Pegawai Redaksi</option>
            <option value="asisten_redaksi">Asisten Redkasi</option>
            <option value="kepala_redaksi">Kepala Redaksi</option>
            <?php if($session->get("peran") == "admin"): ?><option value="admin">Administrator</option> <?endif;?>
        </select>
    </div>
  </div>
</div>
</p>
<p>
<div class="card">
  <div class="card-header">Data Diri</div>
  <div class="card-body">
    <div class="form-group">
        <label for="nama_lengkap">Nama Lengkap:</label>
        <input type="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="" id="nama_lengkap">
    </div>

    <div class="form-group">
        <label for="tanggal_lahir">Tanggal Lahir:</label>
        <input type="tanggal_lahir" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value="<?=date("Y-m-d");?>">
    </div>

  </div>
</div>
</p>

<p>
<button type="submit" class="btn btn-primary btn-block">Tambah</button>
</p>

</form>
