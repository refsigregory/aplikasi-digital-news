<div class="card">
  <div class="card-header">Pengguna</div>
  <div class="card-body">
<p>
<a class="btn btn-primary" href="/tambah_pengguna">TAMBAH</a>
</p>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>No</th>
            <th>Username</th>
            <th>Email</th>
            <th>Nama Lengkap</th>
            <th>Tanggal Lahir</th>
            <th>Peran</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if(count($pengguna) > 0):
        $no = 1;
        foreach($pengguna as $row):
    ?>
    <?php if($row->peran == "admin"): if($session->get('peran') == 'admin'): /* Selain admin tidak boleh edit data user admin */?>
        <tr>
            <td><?=$no;?></td>
            <td><?=$row->username;?></td>
            <td><?=$row->email;?></td>
            <td><?=$row->nama_lengkap;?></td>
            <td><?=$row->tanggal_lahir;?></td>
            <td><?=ucwords(str_replace("_"," ",$row->peran));?></td>
            <td>
                <a class="btn btn-info" href="/edit_pengguna/<?=$row->id_pengguna;?>">Ubah</a>
                <a class="btn btn-danger" href="/hapus_pengguna/<?=$row->id_pengguna;?>">Hapus</a>
            </td>
        </tr>
    <?php endif;?>

    <?php else:?>

        <tr>
            <td><?=$no;?></td>
            <td><?=$row->username;?></td>
            <td><?=$row->email;?></td>
            <td><?=$row->nama_lengkap;?></td>
            <td><?=$row->tanggal_lahir;?></td>
            <td><?=ucwords(str_replace("_"," ",$row->peran));?></td>
            <td>
                <a class="btn btn-info" href="/edit_pengguna/<?=$row->id_pengguna;?>">Ubah</a>
                <a class="btn btn-danger" href="/hapus_pengguna/<?=$row->id_pengguna;?>">Hapus</a>
            </td>
        </tr>

    <?php endif;?>
    <?php
    $no++;
        endforeach;
    endif;
    ?>
    </tbody>
</table>


</div>
</div>