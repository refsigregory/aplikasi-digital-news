<form method="post" action="/edit_berita" enctype="multipart/form-data">
<input type="hidden" name="id_berita" value="<?=$berita->id_berita;?>">
<div class="card">
  <div class="card-header">Berita Baru</div>
  <div class="card-body">
    <div class="form-group">
        <input type="hidden" name="thumbnail_berita" value="<?=$berita->thumbnail_berita;?>" >
        <input type="hidden" name="file_berita_lama" value="<?=$berita->file_berita;?>" >
        <img style="max-width: 80%" src="/files/uploads/<?=$berita->thumbnail_berita;?>">
    </div>

    <div class="form-group">
        <label for="file_berita">File Berita: <input type="checkbox" name="ganti_file" value="true"> Ganti File</label>
        <input type="file" name="file_berita" class="form-control" placeholder="" id="file_berita">
    </div>

    <?php if(true):?>
    <div class="form-group">
        <label for="edisi">Edisi:</label>
        <input type="edisi" name="edisi" class="form-control" placeholder="" id="edisi" value="<?=$berita->edisi;?>"> 
    </div>

    <div class="form-group">
        <label for="halaman">Halaman:</label>
        <input type="halaman" name="halaman" class="form-control" placeholder="" id="halaman" value="<?=$berita->halaman;?>">
    </div>

    <div class="form-group">
        <label for="penerbit">Peneribit:</label>
        <input type="penerbit" name="penerbit" class="form-control" placeholder="" id="penerbit" value="<?=$berita->penerbit;?>">
    </div>
    <?php endif;?>

    <?php if($session->get('peran') == "asisten_redaksi" || $session->get('peran') == "admin"):?>
    <div class="form-group">
        <label for="tanggal_lahir">Status:</label>
        <select name="status_berita" class="form-control">
            <option value="aktif"<?=($berita->status_berita == 'aktif' ? ' selected="selected"' : '');?>>Aktif</option> 
            <option value="nonaktif"<?=($berita->status_berita == 'nonaktif' ? ' selected="selected"' : '');?>>Nonaktif</option> 
        </select>
    </div>
    <?php endif;?>

  </div>
</div>
</p>

<p>
<button type="submit" class="btn btn-primary btn-block">Simpan</button>
</p>

</form>
