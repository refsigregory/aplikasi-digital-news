<div class="card">
  <div class="card-header">Berita</div>
  <div class="card-body">
<p>
<a class="btn btn-primary" href="/tambah_berita">TAMBAH</a>
</p>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>No</th>
            <th>Preview</th>
            <th>Edisi</th>
            <th>Halaman</th>
            <th>Penerbit</th>
            <th>File</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if(count($berita) > 0):
        $no = 1;
        foreach($berita as $row):
    ?>
    <?php if($session->get('peran') != "admin" && $session->get('peran') != "kepala_redaksi"): /* Selain admin dan kepala redaksi hanya bisa lihat punya sendiri */?>
        <?php if($row->diposting_oleh == $session->get("id")):?>
        <tr>
            <td><?=$no;?></td>
            <td><img src="/files/uploads/<?=$row->thumbnail_berita;?>" width="250px" style="border:solid 1px;" /></td>
            <td><?=$row->edisi;?></td>
            <td><?=$row->halaman;?></td>
            <td><?=$row->penerbit;?></td>
            <td><a href="/files/uploads/<?=$row->file_berita;?>" target="_blank">Lihat</a></td>
            <td><?=$row->status_berita;?></td>
            <td>
                <a class="btn btn-info" href="/edit_berita/<?=$row->id_berita;?>">Ubah</a>
                <a class="btn btn-danger" href="/hapus_berita/<?=$row->id_berita;?>">Hapus</a>
            </td>
        </tr>
        <?php endif;?>
    <?php else:?>
        <tr>
            <td><?=$no;?></td>
            <td><img src="/files/uploads/<?=$row->thumbnail_berita;?>" width="250px" style="border:solid 1px;" /></td>
            <td><?=$row->edisi;?></td>
            <td><?=$row->halaman;?></td>
            <td><?=$row->penerbit;?></td>
            <td><a href="/files/uploads/<?=$row->file_berita;?>" target="_blank">Lihat</a></td>
            <td><?=$row->status_berita;?></td>
            <td>
                <a class="btn btn-info" href="/edit_berita/<?=$row->id_berita;?>">Ubah</a>
                <a class="btn btn-danger" href="/hapus_berita/<?=$row->id_berita;?>">Hapus</a>
            </td>
        </tr>
    <?php endif;?>

    <?php
    $no++;
        endforeach;
    endif;
    ?>
    </tbody>
</table>

</div>
</div>