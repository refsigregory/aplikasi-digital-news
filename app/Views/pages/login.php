
<div class="card">
  <div class="card-header">Login</div>
  <div class="card-body">
    <form method="post" action="/login">
    <div class="form-group">
        <label for="username">Username:</label>
        <input type="username" name="username" class="form-control" placeholder="" id="username">
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" placeholder="" id="pwd">
    </div>
    <button type="submit" class="btn btn-primary">Masuk</button>
    </form>
  </div>
</div>