<form method="post" action="/ubah_profil">
<input type="hidden" name="id_pengguna" value="<?=$user->id_pengguna;?>"/>
<p>
<div class="card">
  <div class="card-header">Ubah Pengguna</div>
  <div class="card-body">
    <div class="form-group">
        <label for="username">Username:</label>
        <input type="username" name="username" class="form-control" placeholder="" id="username" value="<?=$user->username;?>">
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" placeholder="" id="pwd" value="<?=$user->password;?>">
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" class="form-control" placeholder="" id="email" value="<?=$user->email;?>">
    </div>

    <div class="form-group">
        <label for="tanggal_lahir">Peran:</label>
        <select name="peran" class="form-control">
            <option value="pengiklan"<?=($user->peran == 'pengiklan' ? ' selected="selected"' : '');?>>Pengiklan</option> 
            <option value="pegawai_redaksi"<?=($user->peran == 'pegawai_redaksi' ? ' selected="selected"' : '');?>>Pegawai Redaksi</option>
            <option value="asisten_redaksi"<?=($user->peran == 'asisten_redaksi' ? ' selected="selected"' : '');?>>Asisten Redkasi</option>
            <option value="kepala_redaksi"<?=($user->peran == 'kepala_redaksi' ? ' selected="selected"' : '');?>>Kepala Redaksi</option>
            <?php if($session->get("peran") == "admin"): ?>
              <option value="admin"<?=($user->peran == 'admin' ? ' selected="selected"' : '');?>>Administrator</option>
            <?php endif;?>
        </select>
    </div>
  </div>
</div>
</p>
<p>
<div class="card">
  <div class="card-header">Data Diri</div>
  <div class="card-body">
    <div class="form-group">
        <label for="nama_lengkap">Nama Lengkap:</label>
        <input type="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="" id="nama_lengkap" value="<?=$user->nama_lengkap;?>">
    </div>

    <div class="form-group">
        <label for="tanggal_lahir">Tanggal Lahir:</label>
        <input type="tanggal_lahir" name="tanggal_lahir" class="form-control" value="<?=$user->tanggal_lahir;?>" id="tanggal_lahir">
    </div>

  </div>
</div>
</p>

<p>
<button type="submit" class="btn btn-primary btn-block">Simpan</button>
</p>

</form>
