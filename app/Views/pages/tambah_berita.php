<form method="post" action="/tambah_berita" enctype="multipart/form-data">
<div class="card">
  <div class="card-header">Berita Baru</div>
  <div class="card-body">
    <div class="form-group">
        <label for="file_berita">File Berita:</label>
        <input type="file" name="file_berita" class="form-control" placeholder="" id="file_berita">
    </div>

    <div class="form-group">
        <label for="edisi">Edisi:</label>
        <input type="edisi" name="edisi" class="form-control" placeholder="" id="edisi">
    </div>

    <div class="form-group">
        <label for="halaman">Halaman:</label>
        <input type="halaman" name="halaman" class="form-control" placeholder="" id="halaman">
    </div>

    <div class="form-group">
        <label for="penerbit">Peneribit:</label>
        <input type="penerbit" name="penerbit" class="form-control" placeholder="" id="penerbit">
    </div>

  </div>
</div>
</p>

<p>
<button type="submit" class="btn btn-primary btn-block">Tambah</button>
</p>

</form>
