<?php 
try {  
$pdf_file = '/home/projects/web/digital-news.projects.ref.si/public_html/public/files/uploads/1603441276_decddb985665795b79a8.pdf'; 

if (! is_readable($pdf_file)) {
    echo 'file not readable';
    exit();
}

$im = new Imagick();
$im->setResolution(300, 300);     //set the resolution of the resulting jpg
$im->readImage($pdf_file.'[0]');    //[0] for the first page
$im->setImageFormat('jpg');
header('Content-Type: image/jpeg');
echo $im;

} catch(Exception $ex)
{
    echo $ex;
}
?>
