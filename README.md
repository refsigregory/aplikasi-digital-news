# Requirements
- Install Composer
- Install Imagick atau PHP-Imagick dan GhostScript (Untuk Auto-Generate Thumbnail PDF)

Linux:

`sudo apt install php-imagick`

`sudo apt install ghostscript`

Cari `policy.xml` dibagian PDF atur jadi `read|write`

# How To Install
* Buka CMD/Powershell (Windows) & Terminal (Linux)

Windows:

`cd C:/xampp/nama_projek`

Linux:

`cd /var/www/html/nama_projek`

* Run `composer install`

* Run `php spark serve` untuk memulai aplikasi.

* Copy file `env` menjadi `.env` dan hilangkan comment pada `app.baseURL` dan `CI_ENVIRONMENT`, pada bisa diubah menjadi `CI_ENVIRONMENT = development` agar dapat menampilkan error (opsional)

Untuk database sesuaikan pada bagian berikut ini:

`database.tests.hostname = localhost`

`database.tests.database = ci4`

`database.tests.username = root`

`database.tests.password = root`

* Untuk membuat table pada database Run `php spark migrate`, gunakan `php spark db:seed PenggunaSeeder` untuk mengisi user pengguna default (opsional).
